#ifndef _CHASSIS_H_
#define _CHASSIS_H_

// sets the drive to a specific power
void driveSet(int left, int right);

// stops the drive
void driveStop();

// moves robot foward at speed for time
void driveTime(int speed, int time);


void startSlewControl();

// init sonar sensors
void initSonar();

// get front sonar
int getBackSonar();

// get back sonar
int getFrontSonar();

#endif
