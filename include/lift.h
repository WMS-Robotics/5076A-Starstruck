#ifndef _LIFT_H_
#define _LIFT_H_


// runs lift at selected value
void liftRun(int power);

// holds lift
void liftHold();

// stops the lift
void liftStop();

// sets the claw to requested power
void clawSet(int power);

#endif
