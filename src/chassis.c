#include "main.h"    // includes API.h and other headers
#include "chassis.h" // redundant, but ensures that the corresponding header file (chassis.h) is included

// drive motor defines
#define	leftFront 3
#define leftBack 2
#define rightFront 6
#define rightBack 7

#define MOTOR_NUM               4    //kNumbOfTotalMotors
#define MOTOR_MAX_VALUE         127
#define MOTOR_MIN_VALUE         (-127)
#define MOTOR_DEFAULT_SLEW_RATE 25      // Default will cause 375mS from full fwd to rev
#define MOTOR_FAST_SLEW_RATE    256     // essentially off
#define MOTOR_TASK_DELAY        15      // task 1/frequency in mS (about 66Hz)
#define MOTOR_DEADBAND          10

// Array to hold requested speed for the motors
int motorReq[ MOTOR_NUM ];

// Array to hold "slew rate" for the motors, the maximum change every time the task
// runs checking current motor speed.
int motorSlew[ MOTOR_NUM ];

void MotorSlewRateTask()
{
    int motorIndex;
    int motorTmp;

    // Initialize stuff
    for(motorIndex=0;motorIndex<MOTOR_NUM;motorIndex++)
        {
        motorReq[motorIndex] = 0;
        motorSlew[motorIndex] = MOTOR_DEFAULT_SLEW_RATE;
        }

    // run task until stopped
    while( true )
        {
        // run loop for every motor
        for( motorIndex=0; motorIndex<MOTOR_NUM; motorIndex++)
            {
            // So we don't keep accessing the internal storage
            motorTmp = motorGet(motorIndex);

            // Do we need to change the motor value ?
            if( motorTmp != motorReq[motorIndex] )
                {
                // increasing motor value
                if( motorReq[motorIndex] > motorTmp )
                    {
                    motorTmp += motorSlew[motorIndex];
                    // limit
                    if( motorTmp > motorReq[motorIndex] )
                        motorTmp = motorReq[motorIndex];
                    }

                // decreasing motor value
                if( motorReq[motorIndex] < motorTmp )
                    {
                    motorTmp -= motorSlew[motorIndex];
                    // limit
                    if( motorTmp < motorReq[motorIndex] )
                        motorTmp = motorReq[motorIndex];
                    }

                // finally set motor
                motorSet(motorIndex, motorTmp);
                }
            }

        // Wait approx the speed of motor update over the spi bus
        wait(MOTOR_TASK_DELAY);
        }
}



// sets drive to requested value
void driveSet(int left, int right) {
/*  motorReq[leftFront] = left;
  motorReq[leftBack] = left;
  motorReq[rightFront] = -right;
  motorReq[rightBack] = -right;
*/
  motorSet(leftFront, left);
  motorSet(leftBack, left);
  motorSet(rightFront, -right);
  motorSet(rightBack, -right);
}

// drive stop
void driveStop() {
  motorSet(leftFront, 0);
  motorSet(leftBack, 0);
  motorSet(rightFront, 0);
  motorSet(rightBack, 0);
}

// moves robot foward at speed for time
void driveTime(int speed, int time) {
  motorSet(leftFront, speed);
  motorSet(leftBack, speed);
  motorSet(rightFront, -speed);
  motorSet(rightBack, -speed);
  wait(time);
  motorSet(leftFront, 0);
  motorSet(leftBack, 0);
  motorSet(rightFront, 0);
  motorSet(rightBack, 0);
}

void startSlewControl() {
  taskCreate(MotorSlewRateTask, TASK_DEFAULT_STACK_SIZE, NULL, TASK_PRIORITY_DEFAULT);
}

int getBackSonar() {
  return ultrasonicGet(backSonar);
}

int getFrontSonar() {
  return ultrasonicGet(frontSonar);
}
