/** @file auto.c
 * @brief File for autonomous code
 *
 * This file should contain the user autonomous() function and any functions related to it.
 *
 * Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/
 *
 * PROS contains FreeRTOS (http://www.freertos.org) whose source code may be
 * obtained from http://sourceforge.net/projects/freertos/files/ or on request.
 */

#include "main.h"
#include "chassis.h"
#include "lift.h"
/*
 * Runs the user autonomous code. This function will be started in its own task with the default
 * priority and stack size whenever the robot is enabled via the Field Management System or the
 * VEX Competition Switch in the autonomous mode. If the robot is disabled or communications is
 * lost,  the autonomous task will be stopped by the kernel. Re-enabling the robot will restart
 * the task, not re-start it from where it left off.
 *
 * Code running in the autonomous task cannot access information from the VEX Joystick. However,
 * the autonomous function can be invoked from another task if a VEX Competition Switch is not
 * available, and it can access joystick information if called in this way.
 *
 * The autonomous task may exit, unlike operatorControl() which should never exit. If it does
 * so, the robot will await a switch to another mode or disable/enable cycle.
 */
void autonomous() {
	/*	motorSet(1, -20);
	liftRun(-127);
	delay(800);
	liftRun(127);
	delay(800);
	liftStop();
	driveTime(127, 550);
	motorSet(1, 100);
	delay(400);
	motorSet(1, 20);

	driveSet(100, -100);
	delay(500);

	driveStop();

	liftRun(-127);
	delay(900);
	driveTime(127, 1400);
	motorSet(1, -127);
	delay(1000);
	motorSet(1, -20);*/

  motorSet(1, -20);
	liftRun(127);
	delay(100);
	liftRun(-100);
	delay(150);
	liftRun(100);
	delay(100);
	liftStop();
	motorSet(1, -100);
	driveTime(127, 200);
	motorSet(1, 100);
	delay(400);
	liftRun(-127);
	delay(900);
	driveTime(127, 1100);
	motorSet(1, -127);
	delay(1000);
	motorSet(1, -20);
	driveSet(-100, -100);
	delay(400);
	liftRun(40);
	delay(40);
	driveSet(100, 100);
	delay(400);
	liftRun(-100);
	delay(200);


	if(selectedAuton == 1 || selectedAuton == 3){
		liftRun(-127);
		wait(800);
		liftRun(127);
		wait(800);
		liftStop();
		clawSet(127);
		wait(500);
		clawSet(20);
		driveTime(127, 500);
		clawSet(-100);
		wait(400);
		liftRun(-127);
		wait(900);
		driveTime(127, 1100);
	  clawSet(127);
		wait(1000);
		clawSet(20);
	}
}
