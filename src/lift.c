#include "main.h"
#include "lift.h"

// lift motor defines
#define liftLeft 4
#define liftLeftB 9
#define liftRight 5
#define liftRightB 8

#define claw 1

#define holdPower 0

void liftRun(int power){
  motorSet(liftLeft, power);
  motorSet(liftLeftB, power);
  motorSet(liftRight, -power);
  motorSet(liftRightB, -power);
}

void liftHold(){
  motorSet(liftLeft, holdPower);
  motorSet(liftLeftB, holdPower);
  motorSet(liftRight, -holdPower);
  motorSet(liftRightB, -holdPower);
}

void liftStop(){
  motorSet(liftLeft, 0);
  motorSet(liftLeftB, 0);
  motorSet(liftRight, 0);
  motorSet(liftRightB, 0);
}

void clawSet(int power) {
  motorSet(claw, power);
}
