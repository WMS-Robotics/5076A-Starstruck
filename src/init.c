/** @file init.c
 * @brief File for initialization code
 *
 * This file should contain the user initialize() function and any functions related to it.
 *
 * Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/
 *
 * PROS contains FreeRTOS (http://www.freertos.org) whose source code may be
 * obtained from http://sourceforge.net/projects/freertos/files/ or on request.
 */

#include "main.h"
#include "chassis.h"
#include "lift.h"

#define frontSonarIn   3
#define frontSonarOut  4

#define backSonarIn  5
#define backSonarOut  6

/*
 * Runs pre-initialization code. This function will be started in kernel mode one time while the
 * VEX Cortex is starting up. As the scheduler is still paused, most API functions will fail.
 *
 * The purpose of this function is solely to set the default pin modes (pinMode()) and port
 * states (digitalWrite()) of limit switches, push buttons, and solenoids. It can also safely
 * configure a UART port (usartOpen()) but cannot set up an LCD (lcdInit()).
 */
void initializeIO() {
}

/*
 * Runs user initialization code. This function will be started in its own task with the default
 * priority and stack size once when the robot is starting up. It is possible that the VEXnet
 * communication link may not be fully established at this time, so reading from the VEX
 * Joystick may fail.
 *
 * This function should initialize most sensors (gyro, encoders, ultrasonics), LCDs, global
 * variables, and IMEs.
 *
 * This function must exit relatively promptly, or the operatorControl() and autonomous() tasks
 * will not start. An autonomous mode selection menu like the pre_auton() in other environments
 * can be implemented in this task if desired.
 */
# define lcdPort uart2

void initialize() {
  backSonar = ultrasonicInit (backSonarIn, backSonarOut);
  frontSonar = ultrasonicInit(backSonarIn, backSonarOut);

  lcdInit(lcdPort);
  lcdClear(lcdPort);

  int page = 1;
  selectedAuton = 0;

  while(selectedAuton) {

    switch (page) {
      case 1:
        lcdSetText(lcdPort, 0, "<AUTON #1>");
        lcdSetText(lcdPort, 1, "Blue Driver");
      case 2:
        lcdSetText(lcdPort, 0, "<AUTON #2>");
        lcdSetText(lcdPort, 1, "Blue Non-Driver");
      case 3:
        lcdSetText(lcdPort, 0, "<AUTON #3>");
        lcdSetText(lcdPort, 1, "Red Driver");
      case 4:
        lcdSetText(lcdPort, 0, "<AUTON #4>");
        lcdSetText(lcdPort, 1, "Red Non-Driver");

      if(lcdReadButtons(lcdPort) == 1) {
        if (page == 1) page = 4;
        else page --;
      }
      else if(lcdReadButtons(lcdPort) == 3) {
        if (page = 4) page = 1;
        else page ++;
      }
      else if(lcdReadButtons(lcdPort) == 2) {
        lcdClear(lcdPort);
        lcdSetText(lcdPort, 0, "Auton Selected");
        selectedAuton = page;
        break;
      }

    }
  }
}
