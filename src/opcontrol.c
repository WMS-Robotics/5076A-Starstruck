/** @file opcontrol.c
 * @brief File for operator control code
 *
 * This file should contain the user operatorControl() function and any functions related to it.
 *
 * Any copyright is dedicated to the Public Domain.
 * http://creativecommons.org/publicdomain/zero/1.0/
 *
 * PROS contains FreeRTOS (http://www.freertos.org) whose source code may be
 * obtained from http://sourceforge.net/projects/freertos/files/ or on request.
 */

// claw motor define
#define claw 8

#include "main.h"
#include "chassis.h"
#include "lift.h"

bool clawClosed = true;

void operatorControl() {

	int power;
	int turn;
	while (1) {
	//	printf("read\n");
	//	printf("%d\n", ultrasonicGet(backSonar) );
		// drive control
		power = joystickGetAnalog(1, 3);
		turn = joystickGetAnalog(1, 1);
		// set motors
		if (abs(power + turn) > 20) {
			driveSet(power + turn, power - turn);
		}
		else {
			driveSet(0, 0);
		}

		// claw control
		if (joystickGetDigital(1, 6, JOY_UP) == true) {
			clawSet(127);
			clawClosed = false;
		}
		else if (joystickGetDigital(1, 6, JOY_DOWN) == true) {
			clawSet(-127);
			clawClosed = true;
		}
		else if (clawClosed == true) clawSet(-20);
		else if (clawClosed == false) clawSet(20);
		else {
			clawSet(0);
		}

		// lift control
		if (joystickGetDigital(1, 5, JOY_UP)== true) {
			liftRun(-127);
		}
		else if (joystickGetDigital(1, 5, JOY_DOWN) == true) {
			liftRun(127);
		}
		else {
			liftHold();
		}

		delay(20);
	}
}
